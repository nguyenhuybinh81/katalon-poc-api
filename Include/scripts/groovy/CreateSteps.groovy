import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When


class CreateSteps {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	//def String name, job
	def Object response;

	//@Given("I want to create a new user with '(.*)' and '(.*)'")
	@Given("I want to create a new user")
	//def I_want_to_write_a_step_with_name(String name, String job) {
	def I_want_to_write_a_step_with_name() {
		//println "NAME1: " + name + ", JOB: " + job
		//this.name = name
		//this.job = job
	}

	@When("I make a POST request with '(.*)' and '(.*)'")
	def make_a_post_request(String name, String job) {
		//response = WS.sendRequest(findTestObject('User/CreateUser', [('name') : name, ('job') : job]))
		response = WS.sendRequest(findTestObject('User/CreateUser', [('baseUrl') : GlobalVariable.baseUrl, ('name') : name, ('job') : job]))
	}

	@Then("I get a 201 response")
	def verify_the_status_code() {
		//println "status: 201"
		WS.verifyResponseStatusCode(response, 201)
	}

	@Then("I verify the created data with '(.*)' and '(.*)'")
	def verify_created_data(String name, String job) {
		//println "RES-NAME: " + name + ", RES-JOB: " + job
		WS.verifyElementPropertyValue(response, 'name', name)
		WS.verifyElementPropertyValue(response, 'job', job)
	}
}