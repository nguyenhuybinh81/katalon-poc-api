@tag
Feature: Create user
  I want to create new user

  @tag1
  Scenario Outline: Create user successfully
    Given I want to create a new user
    When I make a POST request with '<name>' and '<job>'
    Then I get a 201 response
    And I verify the created data with '<name>' and '<job>'

    Examples: 
      | name         | job                 |
      | Micheal Binh | Automation Engineer |
      | Chandra Dau  | QA Lead             |
