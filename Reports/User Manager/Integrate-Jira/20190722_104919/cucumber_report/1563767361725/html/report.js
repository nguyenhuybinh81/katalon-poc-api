$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("/Volumes/DATA/project/katalon-poc-api/Include/features/PIX-1435.feature");
formatter.feature({
  "name": "Create user",
  "description": "  I want to create new user",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@tag"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Create user successfully",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to create a new user",
  "keyword": "Given "
});
formatter.step({
  "name": "I make a POST request with \u0027\u003cname\u003e\u0027 and \u0027\u003cjob\u003e\u0027",
  "keyword": "When "
});
formatter.step({
  "name": "I get a 201 response",
  "keyword": "Then "
});
formatter.step({
  "name": "I verify the created data with \u0027\u003cname\u003e\u0027 and \u0027\u003cjob\u003e\u0027",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "job"
      ]
    },
    {
      "cells": [
        "Micheal Binh",
        "Automation Engineer"
      ]
    },
    {
      "cells": [
        "Chandra Dau",
        "QA Lead"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create user successfully",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to create a new user",
  "keyword": "Given "
});
formatter.match({
  "location": "CreateSteps.I_want_to_write_a_step_with_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I make a POST request with \u0027Micheal Binh\u0027 and \u0027Automation Engineer\u0027",
  "keyword": "When "
});
formatter.match({
  "location": "CreateSteps.make_a_post_request(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get a 201 response",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateSteps.verify_the_status_code()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I verify the created data with \u0027Micheal Binh\u0027 and \u0027Automation Engineer\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "CreateSteps.verify_created_data(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Create user successfully",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to create a new user",
  "keyword": "Given "
});
formatter.match({
  "location": "CreateSteps.I_want_to_write_a_step_with_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I make a POST request with \u0027Chandra Dau\u0027 and \u0027QA Lead\u0027",
  "keyword": "When "
});
formatter.match({
  "location": "CreateSteps.make_a_post_request(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I get a 201 response",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateSteps.verify_the_status_code()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I verify the created data with \u0027Chandra Dau\u0027 and \u0027QA Lead\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "CreateSteps.verify_created_data(String,String)"
});
formatter.result({
  "status": "passed"
});
});